<?php

namespace App\Exceptions;

use Exception;
use Throwable;

class GeneralException extends Exception
{
    /**
     * @var $type string
     */
    private $type;

    public function __construct($message, Throwable $previous = null, $type = 'message')
    {
        parent::__construct($message, 0, $previous);
        $this->type = $type;
    }

    /**
     * Get GeneralException type
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }
}