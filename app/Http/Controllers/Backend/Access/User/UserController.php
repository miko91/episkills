<?php

namespace App\Http\Controllers\Backend\Access\User;

use App\Exceptions\GeneralException;
use App\Models\Access\User\User;
use App\Http\Controllers\Controller;
use App\Repositories\Backend\Access\User\UserRepository;
use App\Http\Requests\Backend\Access\User\StoreUserRequest;
use App\Http\Requests\Backend\Access\User\ManageUserRequest;
use App\Http\Requests\Backend\Access\User\UpdateUserRequest;
use App\Repositories\Backend\Business\Formation\FormationRepository;

class UserController extends Controller
{
    /**
     * @var $user UserRepository
     */
    private $user;

    /**
     * @var $formation FormationRepository
     */
    private $formation;

    /**
     * UserController constructor.
     * @param UserRepository $userRepository
     * @param FormationRepository $formationRepository
     */
    public function __construct(UserRepository $userRepository, FormationRepository $formationRepository)
    {
        $this->user = $userRepository;
        $this->formation = $formationRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @param ManageUserRequest $request
     * @return \Illuminate\Http\Response
     */
    public function index(ManageUserRequest $request)
    {
        return view('backend.access.user.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param ManageUserRequest $request
     * @return \Illuminate\Http\Response
     */
    public function create(ManageUserRequest $request)
    {
        return view('backend.access.user.create')->withFormations($this->formation->getForSelect());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreUserRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreUserRequest $request)
    {
        $this->user->create($request->all());
        return redirect()->route('admin.access.users.index')
            ->withNotification([
                'title' => 'Ajout d\'utilisateur',
                'type' => 'success',
                'message' => 'Utilisateur ajouté avec succès'
            ]);
    }

    /**
     * Display the specified resource.
     *
     * @param ManageUserRequest $request
     * @param  \App\Models\Access\User\User $user
     * @return \Illuminate\Http\Response
     */
    public function show(ManageUserRequest $request, User $user)
    {
        return view('backend.access.user.show')->withUser($user);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param ManageUserRequest $request
     * @param  \App\Models\Access\User\User $user
     * @return \Illuminate\Http\Response
     */
    public function edit(ManageUserRequest $request, User $user)
    {
        return view('backend.access.user.edit')->withUser($user)->withFormations($this->formation->getForSelect());
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateUserRequest $request
     * @param  \App\Models\Access\User\User $user
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateUserRequest $request, User $user)
    {
        $this->user->update($user, $request->all());
        return redirect()->route('admin.access.users.index')
            ->withNotification([
                'title' => 'Modification d\'un utilisateur',
                'type' => 'success',
                'message' => 'Utilisateur modifié avec succès'
            ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param ManageUserRequest $request
     * @param  \App\Models\Access\User\User $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(ManageUserRequest $request, User $user)
    {
        try {
            $this->user->delete([$user->id]);
            return response()->json([
                'title' => 'Suppression d\'utilisateur',
                'message' => 'L\'utilisateur sélectionné a été correctement supprimé',
                'type' => 'success'
            ]);
        } catch (GeneralException $exception) {
            return response()->json([
                'title' => 'Suppression d\'utilisateur',
                'message' => $exception->getMessage(),
                'type' => 'danger'
            ]);
        }
    }

    public function deleteMany(ManageUserRequest $request)
    {
        try {
            $this->user->delete($request->input('elements'));
            return response()->json([
                'title' => 'Suppression d\'utilisateur',
                'message' => 'Tous les utilisateurs sélectionnés ont été correctement supprimés',
                'type' => 'success'
            ]);
        } catch (GeneralException $exception) {
            return response()->json([
                'title' => 'Suppression d\'utilisateur',
                'message' => $exception->getMessage(),
                'type' => 'danger'
            ]);
        }
    }
}
