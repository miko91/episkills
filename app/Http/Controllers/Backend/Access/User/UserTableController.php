<?php

namespace App\Http\Controllers\Backend\Access\User;

use App\Http\Controllers\Controller;
use App\Http\Requests\Backend\Access\User\ManageUserRequest;
use App\Repositories\Backend\Access\User\UserRepository;

class UserTableController extends Controller
{
    /**
     * @var $user UserRepository
     */
    private $user;

    /**
     * UserTableController constructor.
     * @param UserRepository $userRepository
     */
    public function __construct(UserRepository $userRepository)
    {
        $this->user = $userRepository;
    }

    public function __invoke(ManageUserRequest $request)
    {
        return response()->json($this->user->getForBootstrapTable());
    }
}
