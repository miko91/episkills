<?php

namespace App\Http\Controllers\Backend\Business\Assessment;

use App\Http\Requests\Backend\Business\Assessment\ManageAssessmentRequest;
use App\Http\Requests\Backend\Business\Assessment\StoreAssessmentRequest;
use App\Http\Requests\Backend\Business\Assessment\UpdateAssessmentRequest;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Business\Assessment\Assessment;
use App\Repositories\Backend\Business\Assessment\AssessmentRepository;

class AssessmentController extends Controller
{
    /**
     * @var $assessment AssessmentRepository
     */
    private $assessment;

    /**
     * AssessmentController constructor.
     * @param AssessmentRepository $assessmentRepository
     */
    public function __construct(AssessmentRepository $assessmentRepository)
    {
        $this->assessment = $assessmentRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @param ManageAssessmentRequest $request
     * @return \Illuminate\Http\Response
     */
    public function index(ManageAssessmentRequest $request)
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param ManageAssessmentRequest $request
     * @return \Illuminate\Http\Response
     */
    public function create(ManageAssessmentRequest $request)
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreAssessmentRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreAssessmentRequest $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param ManageAssessmentRequest $request
     * @param  \App\Models\Business\Assessment\Assessment $assessment
     * @return \Illuminate\Http\Response
     */
    public function show(ManageAssessmentRequest $request, Assessment $assessment)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param ManageAssessmentRequest $request
     * @param  \App\Models\Business\Assessment\Assessment $assessment
     * @return \Illuminate\Http\Response
     */
    public function edit(ManageAssessmentRequest $request, Assessment $assessment)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateAssessmentRequest $request
     * @param  \App\Models\Business\Assessment\Assessment $assessment
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateAssessmentRequest $request, Assessment $assessment)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param ManageAssessmentRequest $request
     * @param  \App\Models\Business\Assessment\Assessment $assessment
     * @return \Illuminate\Http\Response
     */
    public function destroy(ManageAssessmentRequest $request, Assessment $assessment)
    {
        //
    }
}
