<?php

namespace App\Http\Middleware;

use Closure;

class BackendAccess
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (! $request->user()->power)
            return redirect()->back(403);

        return $next($request);
    }
}
