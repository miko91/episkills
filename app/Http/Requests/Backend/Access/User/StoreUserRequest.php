<?php

namespace App\Http\Requests\Backend\Access\User;

class StoreUserRequest extends ManageUserRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return parent::authorize();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|string|min:2',
            'first_name' => 'required|string|min:2',
            'email' => 'required|email|unique:users,email',
            'type' => 'required|in:' . implode(',', array_keys(config('business.user_types'))),
            'formation' => 'sometimes|required|integer|exists:formations,id',
            'promotion' => 'required_with:formation|date_format:Y'
        ];
    }
}
