<?php

namespace App\Models\Access\User\Traits\Attribute;

trait UserAttribute
{
    public function getPowerLabelAttribute()
    {
        return $this->power ? '<span class="label label-mint">Oui</span>' : '<span class="label label-danger">Non</span>';
    }

    public function getViewButton()
    {
        return '<a class="btn btn-xs btn-info" href="'. route('admin.access.users.show', ['user' => $this->id]) .'"
                data-toggle="tooltip" data-container="body" data-placement="top" title="Voir">
                    <i class="fa fa-search"></i>
                </a>';
    }

    public function getUpdateButton()
    {
        return '<a class="btn btn-xs btn-primary" href="'. route('admin.access.users.edit', ['user' => $this->id]) .'"
                data-toggle="tooltip" data-container="body" data-placement="top" title="Éditer">
                    <i class="fa fa-edit"></i>
                </a>';
    }

    public function getActionsAttribute()
    {
        return $this->getViewButton() . ' ' . $this->getUpdateButton();
    }
}
