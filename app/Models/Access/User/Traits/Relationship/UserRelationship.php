<?php

namespace App\Models\Access\User\Traits\Relationship;

trait UserRelationship
{
    /**
     * The optional formation
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function formation()
    {
        return $this->belongsTo('App\Models\Business\Formation\Formation');
    }

    /**
     * Get the student internships
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function internshipsAsStudent()
    {
        return $this->hasMany('App\Models\Business\Internship\Internship', 'student_id');
    }

    /**
     * Get the tutor internships
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function internshipsAsTutor()
    {
        return $this->hasMany('App\Models\Business\Internship\Internship', 'tutor_id');
    }

    /**
     * Get the master internships
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function internshipsAsMaster()
    {
        return $this->hasMany('App\Models\Business\Internship\Internship', 'master_id');
    }

    /**
     * Get the students assessments
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function student_assessments()
    {
        return $this->hasMany('App\Models\Business\Assessment\Assessment', 'student_id');
    }

    /**
     * Get the evaluator assessments
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function evaluator_assessments()
    {
        return $this->hasMany('App\Models\Business\Assessment\Assessment', 'evaluator_id');
    }
}
