<?php

namespace App\Models\Business\Assessment;

use Illuminate\Database\Eloquent\Model;
use App\Models\Business\Assessment\Traits\Attribute\AssessmentAttribute;
use App\Models\Business\Assessment\Traits\Relationship\AssessmentRelationship;

class Assessment extends Model
{
    use AssessmentAttribute,
        AssessmentRelationship;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'opinion', 'student_qualities', 'student_defects', 'impression', 'period', 'year', 'completed', 'enable_auto',
        'student_id', 'evaluator_id', 'internship_id',
    ];
}
