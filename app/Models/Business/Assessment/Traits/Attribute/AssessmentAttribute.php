<?php

namespace App\Models\Business\Assessment\Traits\Attribute;

trait AssessmentAttribute
{
    /**
     * Is the assessment about internship or academic skills
     * @return bool
     */
    public function isOnInternship()
    {
        return $this->internship_id !== null;
    }
}
