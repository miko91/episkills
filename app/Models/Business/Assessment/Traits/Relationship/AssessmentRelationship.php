<?php

namespace App\Models\Business\Assessment\Traits\Relationship;

trait AssessmentRelationship
{
    /**
     * Get the assessment's student
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function student()
    {
        return $this->belongsTo('App\Models\Access\User\User', 'student_id');
    }

    /**
     * Get the assessment's evaluator
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function evaluator()
    {
        return $this->belongsTo('App\Models\Access\User\User', 'evaluator_id');
    }

    /**
     * Get the optional assessment's internship
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function internship()
    {
        return $this->belongsTo('App\Models\Business\Internship\Internship', 'internship_id');
    }

    /**
     * Get the evaluated skills with pivot values `evaluator_grade` and `student_grade`
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function skills()
    {
        return $this->belongsToMany('App\Models\Business\Skill\Skill')->withPivot('evaluator_grade', 'student_grade');
    }
}
