<?php

namespace App\Models\Business\Category;

use Illuminate\Database\Eloquent\Model;
use App\Models\Business\Category\Traits\Attribute\CategoryAttribute;
use App\Models\Business\Category\Traits\Relationship\CategoryRelationship;

class Category extends Model
{
    use CategoryAttribute,
        CategoryRelationship;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'index',
    ];
}
