<?php

namespace App\Models\Business\Category\Traits\Relationship;

trait CategoryRelationship
{
    /**
     * Get the associated skills
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function skills()
    {
        return $this->hasMany('App\Models\Business\Skill\Skill', 'category_id');
    }
}
