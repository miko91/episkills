<?php

namespace App\Models\Business\Company;

use Illuminate\Database\Eloquent\Model;
use App\Models\Business\Company\Traits\Attribute\CompanyAttribute;
use App\Models\Business\Company\Traits\Relationship\CompanyRelationship;

class Company extends Model
{
    use CompanyAttribute,
        CompanyRelationship;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'description',
    ];
}
