<?php

namespace App\Models\Business\Company\Traits\Relationship;

trait CompanyRelationship
{
    /**
     * Get the associated internships
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function internships()
    {
        return $this->hasMany('App\Models\Business\Internship\Internship', 'company_id');
    }
}
