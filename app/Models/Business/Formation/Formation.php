<?php

namespace App\Models\Business\Formation;

use Illuminate\Database\Eloquent\Model;
use App\Models\Business\Formation\Traits\Attribute\FormationAttribute;
use App\Models\Business\Formation\Traits\Relationship\FormationRelationship;

class Formation extends Model
{
    use FormationAttribute,
        FormationRelationship;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'description',
    ];
}
