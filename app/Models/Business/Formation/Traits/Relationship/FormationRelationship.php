<?php

namespace App\Models\Business\Formation\Traits\Relationship;

trait FormationRelationship
{
    /**
     * Get the associated students
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function students()
    {
        return $this->hasMany('App\Models\Access\User\User', 'formation_id');
    }

    /**
     * Get the associated skills
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function skills()
    {
        return $this->belongsToMany('App\Models\Business\Skill\Skill');
    }
}
