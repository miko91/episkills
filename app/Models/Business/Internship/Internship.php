<?php

namespace App\Models\Business\Internship;

use Illuminate\Database\Eloquent\Model;
use App\Models\Business\Internship\Traits\Attribute\InternshipAttribute;
use App\Models\Business\Internship\Traits\Relationship\InternshipRelationship;

class Internship extends Model
{
    use InternshipAttribute,
        InternshipRelationship;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'alternation', 'type', 'begin_at', 'end_at', 'indemnity', 'student_id', 'tutor_id', 'master_id', 'company_id',
    ];
}
