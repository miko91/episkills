<?php

namespace App\Models\Business\Internship\Traits\Relationship;

trait InternshipRelationship
{
    /**
     * Get the associated student
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function student()
    {
        return $this->belongsTo('App\Models\Access\User\User', 'student_id');
    }

    /**
     * Get the associated tutor
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function tutor_id()
    {
        return $this->belongsTo('App\Models\Access\User\User', 'tutor_id');
    }

    /**
     * Get the associated master of internship
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function master_id()
    {
        return $this->belongsTo('App\Models\Access\User\User', 'master_id');
    }
}
