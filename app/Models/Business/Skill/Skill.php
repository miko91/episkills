<?php

namespace App\Models\Business\Skill;

use Illuminate\Database\Eloquent\Model;
use App\Models\Business\Skill\Traits\Attribute\SkillAttribute;
use App\Models\Business\Skill\Traits\Relationship\SkillRelationship;

class Skill extends Model
{
    use SkillAttribute,
        SkillRelationship;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'description', 'main_index', 'type', 'type_index', 'sub_type', 'sub_type_index', 'category_id',
    ];
}
