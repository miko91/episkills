<?php

namespace App\Models\Business\Skill\Traits\Attribute;

trait SkillAttribute
{
    public function getNomenclatureAttribute()
    {
        if ($this->category_id !== null && $this->category === null)
            $this->load('category');

        if ($this->category === null) {
            if ($this->main_index === null)
                return null;
            return 'CG.' . $this->main_index;
        } else {
            if ($this->type === null || $this->type_index === null || $this->sub_type === null ||
                $this->sub_type_index === null || $this->main_index === null)
                return null;
            return 'C.' . $this->category->index . '_' . $this->type . '.' . $this->type_index . '.' .
                $this->sub_type . $this->sub_type_index;
        }
    }
}
