<?php

namespace App\Models\Business\Skill\Traits\Relationship;

trait SkillRelationship
{
    /**
     * Get the associated formations
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function formations()
    {
        return $this->belongsToMany('App\Models\Business\Formation\Formation');
    }

    /**
     * Get the associated assessments with pivot values `evaluator_grade` and `student_grade`
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function assessments()
    {
        return $this->belongsToMany('App\Models\Business\Assessment\Assessment')->withPivot('evaluator_grade', 'student_grade');
    }
}
