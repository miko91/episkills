<?php

namespace App\Repositories\Backend\Access\User;

use App\Models\Access\User\User;
use App\Notifications\Frontend\Auth\UserIsCreated;
use App\Repositories\BaseRepository;

class UserRepository extends BaseRepository
{
    const MODEL = User::class;

    public function getForBootstrapTable()
    {
        return $this->getAll();
    }

    public function create(array $data)
    {
        $user = self::MODEL;
        $user = new $user;

        $user->name = $data['name'];
        $user->first_name = $data['first_name'];
        $user->email = $data['email'];
        $user->type = $data['type'];
        if (isset($data['promotion']))
            $user->promotion = $data['promotion'];
        if (isset($data['power']))
            $user->power = $data['power'] == 'on';

        $password = str_random(8, true);

        $user->password = bcrypt($password);

        $this->transaction(function () use ($user, $data) {
            $user->save();

            if (isset($data['formation']))
                $user->formation()->associate($data['formation']);
        }, 'Une erreur technique est survenue', 'notification');

        $user->notify(new UserIsCreated($password));
    }

    public function update(User $user, array $data)
    {
        $user->name = $data['name'];
        $user->first_name = $data['first_name'];
        $user->email = $data['email'];
        $user->type = $data['type'];
        $user->promotion = isset($data['promotion']) ? $data['promotion'] : null;
        $user->power = isset($data['power']) && $data['power'] == 'on';

        $this->transaction(function () use ($user, $data) {
            $user->save();

            if (isset($data['formation']))
                $user->formation()->associate($data['formation']);
            else
                $user->formation()->dissociate();
        }, 'Une erreur technique est survenue', 'notification');
    }

    public function delete(array $data)
    {
        $this->transaction(function () use ($data) {
            $deleted = User::destroy($data);
            if (count($data) !== $deleted)
                throw new \Exception('Tous les n\'ont pas été supprimés');
        }, 'Une erreur technique est survenue', 'notification');
    }
}
