<?php

namespace App\Repositories\Backend\Business\Assessment;

use App\Repositories\BaseRepository;
use App\Models\Business\Assessment\Assessment;

class AssessmentRepository extends BaseRepository
{
    const MODEL = Assessment::class;
}
