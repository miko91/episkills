<?php

namespace App\Repositories\Backend\Business\Category;

use App\Repositories\BaseRepository;
use App\Models\Business\Category\Category;

class CategoryRepository extends BaseRepository
{
    const MODEL = Category::class;
    const KEY = 'name';
    const VALUE = 'id';
}
