<?php

namespace App\Repositories\Backend\Business\Company;

use App\Repositories\BaseRepository;
use App\Models\Business\Company\Company;

class CompanyRepository extends BaseRepository
{
    const MODEL = Company::class;
}
