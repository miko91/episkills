<?php

namespace App\Repositories\Backend\Business\Formation;

use App\Repositories\BaseRepository;
use App\Models\Business\Formation\Formation;

class FormationRepository extends BaseRepository
{
    const MODEL = Formation::class;
    const KEY = 'name';
    //const VALUE = 'id';
}
