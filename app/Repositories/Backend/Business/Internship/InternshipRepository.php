<?php

namespace App\Repositories\Backend\Business\Internship;

use App\Repositories\BaseRepository;
use App\Models\Business\Internship\Internship;

class InternshipRepository extends BaseRepository
{
    const MODEL = Internship::class;
}
