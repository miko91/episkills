<?php

namespace App\Repositories\Backend\Business\Skill;

use App\Models\Business\Skill\Skill;
use App\Repositories\BaseRepository;

class SkillRepository extends BaseRepository
{
    const MODEL = Skill::class;
    const KEY = 'name';
    const VALUE = 'id';
}
