<?php

namespace App\Repositories;

use App\Exceptions\GeneralException;
use Throwable;
use Illuminate\Support\Facades\DB;

class BaseRepository
{
    /**
     * @return mixed
     */
    public function getAll()
    {
        return $this->query()->get();
    }

    /**
     * @return mixed
     */
    public function getCount()
    {
        return $this->query()->count();
    }

    /**
     * @param $id
     *
     * @return mixed
     */
    public function find($id)
    {
        return $this->query()->find($id);
    }

    /**
     * @return mixed
     */
    public function query()
    {
        return call_user_func(static::MODEL.'::query');
    }

    protected function transaction($callback, $message, $type = 'message', $attempts = 1)
    {
        try {
            DB::transaction($callback, $attempts);
        } catch (Throwable $throwable) {
            throw new GeneralException($message, $throwable, $type);
        }
    }

    public function getForSelect()
    {
        if (! defined(static::KEY) || ! defined(static::VALUE))
            throw new GeneralException('const KEY or VALUE is not defined in <strong>' . addslashes(static::class) .'</strong>', null, 'notification');
        $key = static::KEY;
        $value = static::VALUE;
        return $this->getAll()->mapWithKeys(function ($item) use ($key, $value) {
            if (! is_array($key))
                $key = [$key];
            $data = [];
            foreach ($key as $attr)
                $data[] = $item->$attr;
            return [implode(' ', $data) => $item->$value];
        });
    }
}