<?php

return [

    /*
    |--------------------------------------------------------------------------
    | User types
    |--------------------------------------------------------------------------
    |
    | This value is the list of user types
    */
    'user_types' => [
        'professor' => 'Professeur', 'professional' => 'Professionnel', 'student' => 'Étudiant'
    ]
];