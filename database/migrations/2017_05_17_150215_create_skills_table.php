<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSkillsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('skills', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->text('description')->nullable();
            $table->integer('main_index')->nullable();
            $table->string('type')->nullable();
            $table->integer('type_index')->nullable();
            $table->string('sub_type')->nullable();
            $table->integer('sub_type_index')->nullable();
            $table->integer('category_id')->unsigned()->nullable();

            $table->foreign('category_id')
                ->references('id')
                ->on('categories')
                ->onDelete('set null');

            $table->unique(['type', 'type_index', 'sub_type', 'sub_type_index', 'category_id'],'nomenclature');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('skills', function (Blueprint $table) {
            $table->dropUnique('nomenclature');
            $table->dropForeign('skills_category_id_foreign');
        });

        Schema::dropIfExists('skills');
    }
}
