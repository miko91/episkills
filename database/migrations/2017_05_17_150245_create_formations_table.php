<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFormationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('formations', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->text('description')->nullable();
        });

        Schema::create('formation_skill', function (Blueprint $table) {
            $table->integer('formation_id')->unsigned();
            $table->integer('skill_id')->unsigned();

            $table->foreign('formation_id')
                ->references('id')
                ->on('formations')
                ->onDelete('cascade');

            $table->foreign('skill_id')
                ->references('id')
                ->on('skills')
                ->onDelete('cascade');
        });

        Schema::table('users', function (Blueprint $table) {
            $table->integer('formation_id')->after('power')->unsigned()->nullable();

            $table->foreign('formation_id')
                ->references('id')
                ->on('formations')
                ->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropForeign('users_formation_id_foreign');
        });

        Schema::table('formation_skill', function (Blueprint $table) {
            $table->dropForeign('formation_skill_formation_id_foreign');
            $table->dropForeign('formation_skill_skill_id_foreign');
        });

        Schema::dropIfExists('formation_skill');

        Schema::dropIfExists('formations');
    }
}
