<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInternshipsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('internships', function (Blueprint $table) {
            $table->increments('id');
            $table->boolean('alternation')->default(0);
            $table->string('type');
            $table->timestamp('begin_at');
            $table->timestamp('end_at');
            $table->decimal('indemnity');
            $table->integer('student_id')->unsigned();
            $table->integer('tutor_id')->unsigned();
            $table->integer('master_id')->unsigned();
            $table->integer('company_id')->unsigned();
            $table->timestamps();

            $table->foreign('student_id')
                ->references('id')->on('users')
                ->onDelete('cascade');
            $table->foreign('tutor_id')
                ->references('id')->on('users')
                ->onDelete('cascade');
            $table->foreign('master_id')
                ->references('id')->on('users')
                ->onDelete('cascade');
            $table->foreign('company_id')
                ->references('id')->on('companies')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('internships', function (Blueprint $table) {
            $table->dropForeign('internships_company_id_foreign');
            $table->dropForeign('internships_master_id_foreign');
            $table->dropForeign('internships_tutor_id_foreign');
            $table->dropForeign('internships_student_id_foreign');
        });

        Schema::dropIfExists('internships');
    }
}
