<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAssessmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('assessments', function (Blueprint $table) {
            $table->increments('id');
            $table->text('opinion')->nullable();
            $table->text('student_qualities')->nullable();
            $table->text('student_defects')->nullable();
            $table->text('impression')->nullable(); // Mauvaise, Moyenne, Assez bonne, Bonne, Très bonne, Exellente
            $table->string('period');
            $table->integer('year');
            $table->boolean('completed')->default(0);
            $table->boolean('enable_auto')->default(0);
            $table->integer('student_id')->unsigned();
            $table->integer('evaluator_id')->unsigned();
            $table->integer('internship_id')->unsigned()->nullable();
            $table->timestamps();

            $table->foreign('student_id')
                ->references('id')->on('users')
                ->onDelete('cascade');
            $table->foreign('evaluator_id')
                ->references('id')->on('users')
                ->onDelete('cascade');
            $table->foreign('internship_id')
                ->references('id')->on('internships')
                ->onDelete('cascade');
        });

        Schema::create('assessment_skill', function (Blueprint $table) {
            $table->integer('assessment_id')->unsigned();
            $table->integer('skill_id')->unsigned();
            $table->integer('evaluator_grade')->nullable();
            $table->integer('student_grade')->nullable();

            $table->foreign('assessment_id')
                ->references('id')
                ->on('assessments')
                ->onDelete('cascade');
            $table->foreign('skill_id')
                ->references('id')
                ->on('skills')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('assessment_skill', function (Blueprint $table) {
            $table->dropForeign('assessment_skill_assessment_id_foreign');
            $table->dropForeign('assessment_skill_skill_id_foreign');
        });

        Schema::dropIfExists('assessment_skill');

        Schema::table('assessments', function (Blueprint $table) {
            $table->dropForeign('assessments_internship_id_foreign');
            $table->dropForeign('assessments_evaluator_id_foreign');
            $table->dropForeign('assessments_student_id_foreign');
        });

        Schema::dropIfExists('assessments');
    }
}
