<?php

use Database\TruncateTable;
use Illuminate\Database\Seeder;

class FormationTableSeeder extends Seeder
{
    use TruncateTable;

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->truncate('formations');

        $formations = [
            ['name' => 'Apping X'],
            ['name' => 'Apping I']
        ];


        DB::table('formations')->insert($formations);
    }
}
