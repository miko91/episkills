<?php

use App\Models\Access\User\User;
use Database\TruncateTable;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    use TruncateTable;

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->truncate('users');

        $users = [
            [
                'name'          => 'admin',
                'first_name'    => 'admin',
                'email'         => 'admin@test.com',
                'password'      => bcrypt('password'),
                'type'          => 'professor',
                'power'         => true,
                'promotion'     => null
            ],
            [
                'name'          => 'pro',
                'first_name'    => 'pro',
                'email'         => 'pro@test.com',
                'password'      => bcrypt('password'),
                'type'          => 'professional',
                'power'         => false,
                'promotion'     => null
            ],
            [
                'name'          => 'student',
                'first_name'    => 'student',
                'email'         => 'student@test.com',
                'password'      => bcrypt('password'),
                'type'          => 'student',
                'power'         => false,
                'promotion'     => 2017
            ],
        ];

        DB::table('users')->insert($users);

        User::find(3)->formation()->associate(1);
    }
}
