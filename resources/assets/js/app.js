/**
 * Core
 */
require('./bootstrap');

/**
 * Nifty
 */
require('./nifty');

/**
 * Plugins
 */
/* Bootstrap-select */
require('bootstrap-select');
/* Bootstrap-table */
require('bootstrap-table');
require('bootstrap-table/dist/locale/bootstrap-table-fr-FR');
/* Bootstrap-datepicker */
require('bootstrap-datepicker');
/* Bootbox */
var bootbox = require('bootbox');

window.icons = {
    refresh: 'fa-refresh',
    toggle: 'fa-toggle-on',
    columns: 'fa-th-list'
};

$.App = {};

$.App.options = {
    bsOptions: {

    }
};

$(function () {
    _init();

    $.App.bs();
    $.App.editUser();

    // Enable bootstrap tooltip
    $('body').tooltip({
        selector: '[data-toggle="tooltip"]'
    });
});

function _init()
{
    $.App.bs = function () {
        var $table = $('.edit-table');
        var $delete = $('#delete-row');
        var $url = $delete.data('url');

        $table.on('check.bs.table uncheck.bs.table check-all.bs.table uncheck-all.bs.table', function () {
            $delete.prop('disabled', !$table.bootstrapTable('getSelections').length);
        });

        $delete.click(function () {
            var $items = $table.bootstrapTable('getSelections');
            var $data = [];
            var $message = '<ul>';
            $items.forEach(function (item) {
                $message += '<li>'+ ( item.name || item.label) + (item.first_name ? (' '  + item.first_name) : '' ) +'</li>';
                $message += '<input type="hidden" value="'+ item.id +'" name="to_delete" />';
                $data.push(item.id);
            });
            $message += '</ul>';
            var $warning = $delete.data('message');
            if ($warning !== null && $warning !== '')
                $message = '<div class="alert alert-danger"><strong>Attention ! </strong>'+ $warning +'</div>' + $message;

            bootbox.dialog({
                title: 'Suppression ' + $delete.data('title'),
                message: $message,
                buttons: {
                    cancel: {
                        label: 'Annuler',
                        className: 'btn btn-default',
                        callback: function () {
                            $(this).hide();
                        }
                    },
                    confirm: {
                        label: 'Confirmer',
                        className: 'btn btn-danger',
                        callback: function () {
                            $.ajax({
                                url: $url,
                                method: 'post',
                                type: 'json',
                                data: {
                                    _token: window.Laravel.csrfToken,
                                    _method: 'DELETE',
                                    elements: $data
                                }
                            }).done(function (data, textStatus, jqXHR) {
                                $table.bootstrapTable('refresh');
                                $.niftyNoty({
                                    title: data.title,
                                    type: data.type,
                                    message: data.message,
                                    container: 'floating',
                                    timer: 5000
                                });
                            }).fail(function (jqXHR, textStatus, errorThrown) {
                                console.log(textStatus);
                                console.log(errorThrown);
                            });
                        }
                    }
                },
                onEscape: true
            });
        });
    };

    $.App.editUser = function () {
        function toggle(hide) {
            var formation = $('select#formation');
            var promotion = $('input#promotion');
            if (hide) {
                formation.selectpicker('val', '');
                promotion.val('');
            }
            promotion.prop('disabled', hide);
            formation.prop('disabled', hide);
            formation.selectpicker('refresh');
        }

        $('select#type').on('loaded.bs.select changed.bs.select', function () {
            var hide = $(this).selectpicker('val') !== 'student';
            toggle(hide);
        });
    };
}