@extends('layouts.auth')

@section('content')
    <div class="panel-body">
        @if (session('status'))
            <div class="alert alert-success">
                {{ session('status') }}
            </div>
        @endif
        <div class="mar-ver pad-btm">
            <h3 class="h4 mar-no">Mot de passe oublié</h3>
        </div>

        <form method="POST" action="{{ route('auth.password.email') }}">
            {{ csrf_field() }}
            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                <input type="email" name="email" id="email" class="form-control" placeholder="Email" value="{{ old('email') }}" autofocus>
                @if ($errors->has('email'))
                    <span class="help-block">
                        <strong>{{ $errors->first('email') }}</strong>
                    </span>
                @endif
            </div>
            <button class="btn btn-success btn-lg btn-block" type="submit">Réinitiliasier mon mot de passe</button>
        </form>
    </div>

    <div class="pad-all">
        <a href="{{ route('auth.login.show') }}" class="btn-link mar-rgt"><i class="fa fa-arrow-left"></i> Connexion</a>
    </div>
@endsection
