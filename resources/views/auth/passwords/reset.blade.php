@extends('layouts.auth')

@section('content')
    <div class="panel-body">
        @if (session('status'))
            <div class="alert alert-success">
                {{ session('status') }}
            </div>
        @endif
        <div class="mar-ver pad-btm">
            <h3 class="h4 mar-no">Réinitialisation de mot de passe</h3>
        </div>

        <form method="POST" action="{{ route('auth.password.update') }}">
            {{ csrf_field() }}

            <input type="hidden" name="token" value="{{ $token }}">

            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                <input type="email" name="email" id="email" class="form-control" placeholder="Email" value="{{ $email or old('email') }}" autofocus>
                @if ($errors->has('email'))
                    <span class="help-block">
                        <strong>{{ $errors->first('email') }}</strong>
                    </span>
                @endif
            </div>
            <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                <input id="password" type="password" class="form-control" name="password" placeholder="password">

                @if ($errors->has('password'))
                    <span class="help-block">
                        <strong>{{ $errors->first('password') }}</strong>
                    </span>
                @endif
            </div>

            <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" placeholder="confirmation">

                @if ($errors->has('password_confirmation'))
                    <span class="help-block">
                        <strong>{{ $errors->first('password_confirmation') }}</strong>
                    </span>
                @endif
            </div>
            <button class="btn btn-success btn-lg btn-block" type="submit">Réinitiliasier mon mot de passe</button>
        </form>
    </div>

    <div class="pad-all">
        <a href="{{ route('auth.login.show') }}" class="btn-link mar-rgt"><i class="fa fa-arrow-left"></i> Connexion</a>
    </div>
@endsection
