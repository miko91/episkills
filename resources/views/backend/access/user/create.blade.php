@extends('layouts.body')

@section('header')
    @component('components.header')
        @slot('title')
            Utilisateurs
        @endslot
        <li><a href="{{ route('admin.access.users.index') }}">Tous les utilisateurs</a></li>
        <li class="active">Ajouter un utilsateur</li>
    @endcomponent
@endsection

@section('page-content')
    @component('components.panel-form', [
        'panel_title' => 'Ajouter un utilisateur',
        'target' => route('admin.access.users.store'),
        'offset' => 2,
        'back' => route('admin.access.users.index'),
        'fields' => [
            [
                'label' => 'Nom',
                'slug' => 'name',
                'type' => 'text',
                'value' => old('name'),
                'validate' => true
            ],
            [
                'label' => 'Prénom',
                'slug' => 'first_name',
                'type' => 'text',
                'value' => old('first_name'),
                'validate' => true
            ],
            [
                'label' => 'Email',
                'slug' => 'email',
                'type' => 'text',
                'value' => old('email'),
                'validate' => true
            ],
            [
                'label' => 'Type',
                'slug' => 'type',
                'type' => 'select',
                'select' => [
                    'values' => array_flip(config('business.user_types')),
                    'multiple' => false
                ],
                'value' => old('type'),
                'validate' => true
            ],
            [
                'label' => 'Formation',
                'slug' => 'formation',
                'type' => 'select',
                'select' => [
                    'values' => $formations,
                    'multiple' => false
                ],
                'value' => old('formation'),
                'validate' => false
            ],
            [
                'label' => 'Promotion',
                'slug' => 'promotion',
                'type' => 'text',
                'value' => old('promotion'),
                'validate' => true
            ],
            [
                'label' => 'Admin',
                'slug' => 'power',
                'type' => 'checkbox',
                'value' => old('power'),
                'validate' => false
            ]
        ]
    ])
    @endcomponent
@endsection
