@extends('layouts.body')

@section('header')
    @component('components.header')
        @slot('title')
            Utilisateurs
        @endslot
        <li><a href="{{ route('admin.access.users.index') }}">Tous les utilisateurs</a></li>
        <li class="active">Modifier un utilisateur</li>
    @endcomponent
@endsection

@section('page-content')
    @component('components.panel-form', [
        'panel_title' => 'Modifier un utilisateur',
        'target' => route('admin.access.users.update', ['user' => $user->id]),
        'offset' => 2,
        'back' => route('admin.access.users.index'),
        'submit' => 'Modifier',
        'submit_class' => 'primary',
        'method' => method_field('PUT'),
        'fields' => [
            [
                'label' => 'Nom',
                'slug' => 'name',
                'type' => 'text',
                'value' => old('name', $user->name),
                'validate' => true
            ],
            [
                'label' => 'Prénom',
                'slug' => 'first_name',
                'type' => 'text',
                'value' => old('first_name', $user->first_name),
                'validate' => true
            ],
            [
                'label' => 'Email',
                'slug' => 'email',
                'type' => 'text',
                'value' => old('email', $user->email),
                'validate' => true
            ],
            [
                'label' => 'Type',
                'slug' => 'type',
                'type' => 'select',
                'select' => [
                    'values' => array_flip(config('business.user_types')),
                    'multiple' => false
                ],
                'value' => old('type', $user->type),
                'validate' => true
            ],
            [
                'label' => 'Formation',
                'slug' => 'formation',
                'type' => 'select',
                'select' => [
                    'values' => $formations,
                    'multiple' => false
                ],
                'value' => old('formation', $user->formation),
                'validate' => false
            ],
            [
                'label' => 'Promotion',
                'slug' => 'promotion',
                'type' => 'text',
                'value' => old('promotion', $user->promotion),
                'validate' => true
            ],
            [
                'label' => 'Admin',
                'slug' => 'power',
                'type' => 'checkbox',
                'value' => old('power', ($user->power ? 'on' : '')),
                'validate' => false
            ]
        ]
    ])
    @endcomponent
@endsection
