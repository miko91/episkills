@extends('layouts.body')

@section('header')
    @component('components.header')
        @slot('title')
            Utilisateurs
        @endslot
        <li class="active">Tous les utilisateurs</li>
    @endcomponent
@endsection

@section('page-content')
    @component('components.panel-table', [
        'panel_title' => 'Tous les utilisateurs',
        'create_url' => route('admin.access.users.create'),
        'url' => route('admin.access.users.get'),
        'delete_title' => 'd\'utilisateurs',
        'delete_url' => route('admin.access.users.delete-many')
    ])
        <th data-field="name" data-sortable="true">Nom</th>
        <th data-field="first_name" data-sortable="true">Prénom</th>
        <th data-field="email" data-sortable="true">Email</th>
        <th data-field="type" data-sortable="true">Type</th>
        <th data-field="power_label" data-sortable="true">Admin</th>
        <th data-field="actions" data-sortable="false">Actions</th>
    @endcomponent
@endsection
