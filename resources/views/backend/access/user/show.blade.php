@extends('layouts.body')

@section('header')
    @component('components.header')
        @slot('title')
            Utilisateurs
        @endslot
        <li><a href="{{ route('admin.access.users.index') }}">Tous les utilisateurs</a></li>
        <li class="active">#{{ $user->id }} - {{ $user->name }} {{ $user->first_name }}</li>
    @endcomponent
@endsection

@section('page-content')
    @component('components.panel-tab', [
        'panel_title' => 'Voir un utilisateur'
    ])
        @slot('tabs')
            <li class="active"><a href="#{{ str_slug('Vue d\'ensemble') }}">Vue d'ensemble</a></li>
        @endslot
        <div class="tab-pane fade in active" id="{{ str_slug('Vue d\'ensemble') }}">
            <table class="table table-striped table-hover">
                <tbody>
                    <tr>
                        <td><strong>Nom</strong></td>
                        <td>{{ $user->name }}</td>
                    </tr>
                    <tr>
                        <td><strong>Prénom</strong></td>
                        <td>{{ $user->first_name }}</td>
                    </tr>
                    <tr>
                        <td><strong>Email</strong></td>
                        <td>{{ $user->email }}</td>
                    </tr>
                    <tr>
                        <td><strong>Type</strong></td>
                        <td>{{ $user->type }}</td>
                    </tr>
                    <tr>
                        <td><strong>Créé le</strong></td>
                        <td>{{ $user->created_at }}</td>
                    </tr>
                </tbody>
            </table>
        </div>
    @endcomponent
@endsection
