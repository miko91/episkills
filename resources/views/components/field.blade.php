<div class="form-group{{ $field['validate'] && $errors->has($field['slug']) ? ' has-error' : '' }}">
    <label for="{{ $field['slug'] }}" class="col-md-{{ $offset }} control-label">{{ $field['label'] }}</label>
    <div class="col-md-{{ 10 - $offset }}">
        @if(in_array($field['type'], ['text', 'email', 'password']))
            <input class="form-control" type="{{ $field['type'] }}" id="{{ $field['slug'] }}" name="{{ $field['slug'] }}" value="{{ $field['value'] }}">
        @elseif($field['type'] == 'select' && isset($field['select']))
            <select class="form-control selectpicker" name="{{ $field['slug'] }}" id="{{ $field['slug'] }}"{{ $field['select']['multiple'] ? ' multiple' : '' }} data-actions-box="true">
                @foreach($field['select']['values'] as $key => $value)
                    <option value="{{ $value }}"{{ $value == $field['value'] ? 'selected' : '' }}>{{ $key }}</option>
                @endforeach
            </select>
        @elseif($field['type'] == 'checkbox')
            <div class="checkbox">
                <input type="checkbox" class="form-control magic-checkbox" id="{{ $field['slug'] }}" name="{{ $field['slug'] }}"{{ $field['value'] == 'on' ? 'checked' : '' }}>
                <label for="{{ $field['slug'] }}"></label>
            </div>
        @endif
        @if($field['validate'] && $errors->has($field['slug']))
            <small class="help-block">{{ $errors->first($field['slug']) }}</small>
        @endif
    </div>
</div>