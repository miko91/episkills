<div class="panel">
    <div class="panel-heading">
        <h3 class="panel-title">{{ $panel_title }}</h3>
    </div>
    <form action="{{ url($target) }}" method="post" class="form-horizontal">
        {{ csrf_field() }}
        {{ $method or '' }}
        <div class="panel-body">
            @foreach($fields as $field)
                @component('components.field', [
                    'field' => $field,
                    'offset' => $offset
                ])
                @endcomponent
            @endforeach
        </div>
        <div class="panel-footer">
            <div class="row">
                <div class="col-md-offset-{{ $offset }} col-md-{{ 12 - $offset }}">
                    <a href="{{ url($back) }}" class="btn btn-default">
                        Annuler
                    </a>
                    <button class="btn btn-{{ $submit_class or 'mint' }}" type="submit">{{ $submit or 'Ajouter' }}</button>
                </div>
            </div>
        </div>
    </form>
</div>