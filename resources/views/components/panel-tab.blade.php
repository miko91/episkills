<div class="panel">
    <div class="panel-heading">
        <div class="panel-control">
            <ul class="nav nav-tabs">
                {{ $tabs }}
            </ul>
        </div>
        <h3 class="panel-title">{{ $panel_title }}</h3>
    </div>
    <div class="panel-body">
        <div class="tab-content">
            {{ $slot }}
        </div>
    </div>
</div>
