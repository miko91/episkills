<div class="panel">
    <div class="panel-heading">
        <h3 class="panel-title">{{ $panel_title }}</h3>
    </div>
    <div class="panel-body">
        <div id="edit-table-toolbar" data-url="{{ $update_url or $url }}">
            <a class="btn btn-mint" href="{{ url($create_url) }}">
                <i class="fa fa-plus"></i>
                Ajouter
            </a>
            <button id="delete-row" class="btn btn-danger" disabled data-title="{{ $delete_title }}" data-message="{{ $delete_warning or '' }}" data-url="{{ $delete_url or '' }}">
                <i class="fa fa-trash"></i>
                Supprimer
            </button>
        </div>

        <table  class="edit-table"
                data-toggle="table"
                data-url="{{ $url }}"
                data-toolbar="#edit-table-toolbar"
                data-search="true"
                data-locale="fr-FR"
                data-show-refresh="true"
                data-icons="icons"
                data-icons-prefix="fa"
                data-page-list="[5, 10, 20]"
                data-page-size="10"
                data-classes="table table-hover table-condensed table-no-bordered"
                data-pagination="true">
            <thead>
            <tr>
                <th data-field="state" data-checkbox="true">ID</th>
                {{ $slot }}
            </tr>
            </thead>
        </table>
    </div>
</div>