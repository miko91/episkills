@component('mail::message')
# Bonjour {{ $user->first_name }}

Vous recevez ce message car une demande de réinitialisation de mot de passe a été faite pour votre compte.

@component('mail::button', ['url' => $url])
Cliquer ici pour réinitialiser votre mot de passe
@endcomponent

A bientôt,<br>
{{ config('app.name') }}

@component('mail::subcopy')
Si vous rencontrez un problème avec le bouton 'Cliquer ici pour réinitialiser votre mot de passe', copiez et collez l'URL suivante dans votre navigateur :
[{{ $url }}]({{ $url }})
@endcomponent
@endcomponent
