@component('mail::message')
# Bonjour {{ $user->first_name }}

Votre compte *{{ config('app.name') }}* vient d'être crée. Voici vos identifiants :
* identfiant : {{ $user->email }}
* password : {{ $password }}

@component('mail::button', ['url' => route('auth.login.show')])
Se connecter
@endcomponent

A bientôt,<br>
{{ config('app.name') }}

@component('mail::subcopy')
Si vous rencontrez un problème avec le bouton 'Se connecter', copiez et collez l'URL suivante dans votre navigateur :
[{{ route('auth.login.show') }}]({{ route('auth.login.show') }})
@endcomponent
@endcomponent
