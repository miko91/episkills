<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('/css/pace.css') }}" rel="stylesheet">
    <script src="{{ asset('/js/pace.js') }}"></script>
</head>
<body>
    @yield('page')
    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}"></script>
    <script type="text/javascript">
        window.Laravel = {!! json_encode([
            'csrfToken' => csrf_token(),
        ]) !!};
        $(function () {
            @if(session('notification'))
                $.niftyNoty({
                    type: '{{ Session::get('notification')['type'] }}',
                    title: '{{ Session::get('notification')['title'] }}',
                    message: '{!! Session::get('notification')['message'] !!}',
                    container: 'floating',
                    timer: 5000
                });
            @endif
        });
    </script>
</body>
</html>
