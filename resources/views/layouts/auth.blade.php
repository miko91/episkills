@extends('layouts.app')

@section('page')
    <div id="container" class="cls-container">
        <!-- BACKGROUND IMAGE -->
        <!--===================================================-->
        <div id="bg-overlay" class="bg-img"></div>

        <div class="cls-content">
            <div class="cls-content-sm panel">
                @yield('content')
            </div>
        </div>
    </div>
@endsection