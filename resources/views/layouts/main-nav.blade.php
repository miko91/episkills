<!--Menu list item-->
<li class="{{ isActiveRoute('frontend.dashboard', 'active-link') }}">
    <a href="{{ route('frontend.dashboard') }}">
        <i class="fa fa-dashboard"></i>
        <span class="menu-title">
            <strong>Dashboard</strong>
        </span>
    </a>
</li>

@admin
<li class="list-divider"></li>

<li class="list-header">Administration</li>

<li class="{{ isActiveRoute('admin.access.users.*', 'active-link') }}">
    <a href="{{ route('admin.access.users.index') }}">
        <i class="fa fa-users"></i>
        <span class="menu-title">
            <strong>Utilisateurs</strong>
        </span>
    </a>
</li>
@endif