<?php

/*
 * Access routes
 */
Route::group([
    'prefix' => 'access',
    'as' => 'access.',
    'namespace' => 'Access'
], function () {

    /*
     * User management
     */
    Route::group(['namespace' => 'User'], function () {
        /*
         * Get data for bootstrap table ajax call
         */
        Route::get('/users/get', 'UserTableController')->name('users.get');

        /*
         * User CRUD
         */
        Route::resource('users', 'UserController');

        /*
         * Handle multiple delete
         */
        Route::delete('users', 'UserController@deleteMany')->name('users.delete-many');
    });
});